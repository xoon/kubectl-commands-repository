# Kubectl Current-Namespace

* Return current namespace (used by some others extra commands)

## Type of completion: __none__

## Usage

```bash
kubectl current-namespace
```

